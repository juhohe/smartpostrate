import Vue from 'vue';
import { Component } from 'vue-property-decorator';
declare var $: any;

class Comment {
    commentContent: string = "";
    objectId: string = "";

}

@Component
export default class CommentInputComponent extends Vue {
    comment: Comment = new Comment();
    newAdded:boolean=false;

    mounted() {
        this.comment.objectId = this.$route.params.id;
    }

    submit() {
        $.ajax({
            url: "api/Comment",
            type: "POST",
            data: JSON.stringify(this.comment),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: (data: any) => {
                this.newAdded = true;
                this.comment.commentContent = "";
                window.setTimeout(() => {
                        this.newAdded = false;
                    },
                    7000);
            }
        });
    }
}
