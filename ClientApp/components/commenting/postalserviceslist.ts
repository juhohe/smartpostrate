import Vue from 'vue';
import { Component } from 'vue-property-decorator';
declare var $: any;

class PostalService {
    comments: Comment[];
    id: string;
    address: Address;
    publicName: PublicName;
}

class Address {
    fi: Fi;
}

class Fi {
    address: string;
}

class PublicName {
    fi: string;
}

@Component
export default class PostalServicesListComponent extends Vue {
    postalservices: PostalService[] = [];

    mounted() {
        $.ajax({
            url: "api/Comment/postalservices",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: (data: PostalService[]) => {
                this.postalservices = data;
            }
        })
    }
}
