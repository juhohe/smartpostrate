import Vue from 'vue';
import { Component } from 'vue-property-decorator';
declare var $: any;


@Component
export default class CommentListComponent extends Vue {
    comments: Comment[] = [];

    mounted() {
        //window.alert(this.comment.CommentContent);
       // $.post("api/Comment",   JSON.stringify(this.comment) );
        $.ajax({
            url: "api/Comment",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: (data: Comment[]) => {
                this.comments = data;
            }
        })
    }
}
