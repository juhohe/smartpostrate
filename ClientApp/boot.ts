import './css/site.css';
import 'bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    { path: '/', component: require('./components/home/home.vue.html') },
    { path: '/commentinput/:id', component: require('./components/commenting/commentinput.vue.html') },
    { path: '/commentlist', component: require('./components/commenting/commentlist.vue.html') },
    { path: '/postalserviceslist', component: require('./components/commenting/postalserviceslist.vue.html') },
    { path: '/fetchdata', component: require('./components/fetchdata/fetchdata.vue.html') }
];

new Vue({
    el: '#app-root',
    router: new VueRouter({ mode: 'history', routes: routes }),
    render: h => h(require('./components/app/app.vue.html'))
});
