﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace SmartPostRate.Controllers
{
    [Route("api/[controller]")]
    public partial class CommentController : Controller
    {
        public static List<Comment> CommentRepo = new List<Comment>();
        public static List<Location> PostalRepo = new List<Location>();
        [HttpPost()]
        public IActionResult Comment([FromBody]Comment commentContent)
        {
            commentContent.Id = Guid.NewGuid().ToString();
           
            CommentRepo.Add(commentContent);
            return Ok(commentContent);
        }

        [HttpGet()]
        public IEnumerable<Comment> Comment()
        {

            return CommentRepo;
        }

        [HttpGet("postalservices")]
        public IEnumerable<Location> ListPostalService()
        {

            var url = "Https://locationservice.posti.com/location?types=SMARTPOST&city=ESPOO";
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                var resp = client.DownloadString(url);
                var services = JsonConvert.DeserializeObject<RootObject>(resp);
                PostalRepo = services.locations;
                foreach (var location in PostalRepo)
                {
                    var comments = CommentRepo.Where(x => x.ObjectId == location.id).ToList();
                    location.Comments = comments;
                }
                
                return PostalRepo;
            }

        }

    }
}