﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartPostRate.Controllers
{
    public class Fi
    {
        public string address { get; set; }
    }

    public class Address
    {
        public Fi fi { get; set; }
    }

    public class PublicName
    {
        public string fi { get; set; }
    }

    public class Location
    {
        public List<Comment> Comments { get; set; }
        public string id { get; set; }
        public Address address { get; set; }
        public PublicName publicName { get; set; }
    }

    public class RootObject
    {
        public List<Location> locations { get; set; }
    }
}
